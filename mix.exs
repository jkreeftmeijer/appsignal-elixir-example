defmodule AppsignalElixirExample.MixProject do
  use Mix.Project

  def project do
    [
      app: :appsignal_elixir_example,
      version: "0.1.0",
      elixir: "~> 1.11-dev",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:appsignal, "1.13.4"},
      {:jason, "~> 1.1"}
    ]
  end
end
