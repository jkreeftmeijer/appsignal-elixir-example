FROM jkreeftmeijer/alpine-elixir-phoenix:latest AS builder

RUN erl -eval '{ok, Version} = file:read_file(filename:join([code:root_dir(), "releases", erlang:system_info(otp_release), "OTP_VERSION"])), io:fwrite(Version), halt().' -noshell
RUN elixir -v

# Set work directory
WORKDIR /usr/src/build

# Set enviroment to production
ENV MIX_ENV=prod

# Cache elixir deps
COPY mix.exs mix.lock ./
RUN mix do deps.get, deps.compile
