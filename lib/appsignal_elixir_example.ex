defmodule AppsignalElixirExample do
  def instrument do
    Appsignal.instrument("instrument", "category.call", fn ->
      :timer.sleep(500)
    end)
  end

  def instrument_with_child do
    Appsignal.instrument("instrument_with_child", "category.call", fn ->
      :timer.sleep(500)

      Appsignal.instrument("child", "category.call", fn ->
        :timer.sleep(500)
      end)
    end)
  end

  def instrument_without_helper do
    span =
      "http_request"
      |> Appsignal.Tracer.create_span()
      |> Appsignal.Span.set_name("instrument_without_helper")
      |> Appsignal.Span.set_attribute("appsignal:category", "category.call")

    :timer.sleep(500)

    Appsignal.Tracer.close_span(span)
  end

  def hello do
    :world
  end
end
